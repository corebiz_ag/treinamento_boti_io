import React, { Component } from 'react'
import {graphql} from 'react-apollo';

import productsQuery from './queries/productsQuery.gql';

class ListProductsComponent extends Component {
  render() {

    const {data: {products,loading}} = this.props;

    return (
        <div>
            <h1>Listagem de produtos</h1>
            {!loading ? (
            <ul>
                {products.map((product, index) => (
                    <li key={index}>
                        <a href={`/${product.linkText}/p`} title={product.productName}>{product.productName}</a>
                    </li>
                ))}
            </ul>
            ) : ''}
        </div>
    )

  }
}

ListProductsComponent.schema = {
    title: 'List Products',
    description: 'A simple List Products component',
    type: 'object',
    properties: {
      category: {
        type: 'string',
        title: 'Categoria'
      }
    }
}

export default graphql(productsQuery, {
    options: (props) => {

        const {category} = props;

        return {
            variables: {
                category: category
            }
        }

    }
})(ListProductsComponent);