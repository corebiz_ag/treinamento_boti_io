import React, { Component } from 'react'

class HelloWorldFirstComponent extends Component {
  render() {
    
    const { hello, params: { name } } = this.props;

    return (
        <div>
            <h1>{hello}, {name.substr(0,1).toUpperCase() + name.substr(1,name.length)}!</h1>
        </div>
    )

  }
}

HelloWorldFirstComponent.defaultProps = {
    hello: "Olá"
}

HelloWorldFirstComponent.schema = {
    title: 'HelloWorld',
    description: 'A simple HelloWorld component',
    type: 'object',
    properties: {
      hello: {
        type: 'string',
        title: 'Cumprimento'
      }
    }
}

export default HelloWorldFirstComponent