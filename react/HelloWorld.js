import React, { Component } from 'react'
import { ExtensionPoint } from 'render'

class HelloWorld extends Component {
  render() {
    return (
        <ExtensionPoint id="container" {...this.props} />
    )
  }
}

export default HelloWorld